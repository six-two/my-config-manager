# My config manager

## Usage

- Check if programs are installed correctly: `mcm check`
- Try to fix installations: `mcm fix -v`
- Update all installed repos: `mcm update`

## Installation

- Clone this repo and `cd` into it
- Install the repositories: `pip install -r ./requirements.txt`
- Import the keys from ./keys/ with `gpg --import`
- Optional: add `<repo>/bin` to your $PATH


### Trust keys (optional)
Git signature checks return the error state `UnknownValidity` when you have imported a key but do not trust it enough.

So you may want to trust all keys used for signatures:
```
gpg --edit-key 65D1BFEC9FACC7866466CDCA34B9DF73C0BEA9F3
[...]
gpg> trust
[...]
Your decision? 5
Do you really want to set this key to ultimate trust? (y/N) y
[...]
gpg> quit
```


### Modify config (optional)
`~/.config/six-two/my-config-manager.yaml` takes precedence above `<repo>/config.yaml`.

```
mkdir -p ~/.config/six-two/
cp config.yaml ~/.config/six-two/my-config-manager.yaml
nano ~/.config/six-two/my-config-manager.yaml
```
