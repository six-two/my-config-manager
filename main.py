#!/usr/bin/env python3
import argparse
import glob
import os
import sys
from typing import Optional
from my_config_manager import load_settings, config_reader, Settings, parse_project_config, fix_project, check_project, clone_repo, update_repo, Config
import my_config_manager as mcm
from my_config_manager.output import SectionOutput, OutputSettings, format_header, color, RED

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
CONFIG_DIR = os.path.join(SCRIPT_DIR, "configs")

ACT_CHECK = ["c", "check"]
ACT_FIX = ["f", "fix"]
ACT_INSTALL = ["i", "install"]
ACT_UPDATE = ["u", "update"]
ACT_ALL = ACT_CHECK + ACT_FIX + ACT_INSTALL + ACT_UPDATE


def perform_action(action: str, output_settings: OutputSettings, settings: Settings, config: Config, output_written: bool) -> bool:
    header = format_header(config, not output_written)
    section = SectionOutput(output_settings, header)
    if action in ACT_CHECK:
        return check_project(section, settings, config)
    elif action in ACT_FIX:
        return fix_project(section, settings, config)
    elif action in ACT_INSTALL:
        return clone_repo(section, settings, config)
    elif action in ACT_UPDATE:
        return update_repo(section, settings, config)
    else:
        raise Exception(f"Unknown action: {action}")


def get_config_path(project_paths: list[str], name: str) -> Optional[str]:
    for folder in project_paths:
        full_path = os.path.join(folder, f"{name}.yaml")
        if os.path.exists(full_path):
            return full_path
    return None


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("-a", "--show-all", action="store_true", help="show all configs (not only the installed ones)")
    ap.add_argument("-d", "--debug", action="store_true", help="show debug outputs")
    ap.add_argument("-n", "--config-name", help="use one of the predefined configs (see <PROJECT_ROOT>/configs/)")
    ap.add_argument("-O", "--hide-ok", action="store_true", help="only show checks that are not ok")
    ap.add_argument("-v", "--verbose", action="store_true", help="show details for failed checks")
    ap.add_argument("action", choices=ACT_ALL)
    args = ap.parse_args()

    action = args.action

    settings = load_settings()

    # for x in settings.project_paths:
    #     print(config_reader.parse_repo_folder_recursive(x))


    if args.config_name:
        if os.path.exists(args.config_name):
            config_paths = [args.config_name]
        else:
            config = get_config_path(settings.project_paths, args.config_name)
            if not config:
                print(f"No config file for project '{args.config_name}' found")
                sys.exit(1)
            else:
                config_paths = [config]
    else:
        config_paths = []
        for folder in settings.project_paths:
            glob_pattern = os.path.join(folder, "*.yaml")
            config_paths += glob.glob(glob_pattern)

    show_command_output = bool(args.verbose)
    show_commands = bool(args.debug)
    hide_ok = bool(args.hide_ok)
    output_settings = OutputSettings(show_command_output=show_command_output, show_commands=show_commands, hide_ok=hide_ok)

    try:
        success = True
        configs = [parse_project_config(path) for path in config_paths]
        for config in configs:
            if not args.show_all and not os.path.exists(config.path) and action not in ACT_INSTALL:
                # Hide not installed projects
                continue

            if not perform_action(action, output_settings, settings, config, True):
                success = False

        if not success:
            sys.exit(1)
    except KeyboardInterrupt:
        aborted = color(RED, 'Aborted')
        print(f"\n{aborted}")

if __name__ == "__main__":
    main()
