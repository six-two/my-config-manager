#!/usr/bin/env python3
import argparse
import subprocess
import sys

def parse_line(line: str) -> str:
    # example: "xz 5.2.5-1"
    parts = line.split()
    return parts[0]

def get_installed_packages() -> list[str]:
    output = subprocess.check_output(["pacman", "-Q"])
    output = output.decode("utf-8")
    return [parse_line(x) for x in output.split("\n") if x]

def check_packages(wanted_list: list[str]) -> None:
    try:
        installed = set(get_installed_packages())
    except:
        print("Failed to get installed packages with 'pacman -Q'")
        sys.exit(1)

    missing = [wanted for wanted in wanted_list if wanted not in installed]
    if missing:
        if len(missing) == 1:
            print("One missing package. Install it with the following command:")
        else:
            print(len(missing), "missing packages. Install them with the following command:")
        print("sudo pacman -S", " ".join(missing))
        sys.exit(2)

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--packages", nargs="+", help="the packages to check for")
    args = ap.parse_args()

    if args.packages:
        check_packages(args.packages)
    else:
        print(ap.format_help())
        sys.exit(1)
