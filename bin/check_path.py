#!/usr/bin/env python3
# Checks if the given folder(s) are in the system path variable
import argparse
import os
import sys

PATH = os.getenv("PATH")
if not PATH:
    print("Could not get PATH variable or PATH is empty")
    sys.exit(1)
PATH = PATH.split(os.pathsep)


def is_in_path_variable(directory: str) -> bool:
    for path_dir in PATH:
        try:
            # Comparing paths is tricky (symlinks and co).
            # So instead I just check, if the directory is the same.
            if os.path.samefile(directory, path_dir):
                return True
        except:
            # One of the files does not exist
            pass
    return False


if __name__ == "__main__":
    ap = argparse.ArgumentParser(
        description="Checks if given directories are in the path. Will return with code 2, if any directory is not in the path."
    )
    ap.add_argument("dirs", nargs="+", help="the directories to check")
    ap.add_argument("-f", "--format", choices=["bash", "pretty"], default="pretty")
    args = ap.parse_args()

    arg_dirs = [os.path.expanduser(d) for d in args.dirs]
    not_in_path = [d for d in arg_dirs if not is_in_path_variable(d)]
    if not_in_path:
        of = args.format
        if of == "bash":
            missing_folders = [os.path.abspath(x) for x in not_in_path]
            path_list = ["$PATH", *missing_folders]
            path = ":".join(path_list)
            print(f'export PATH="{path}"')
        elif of == "pretty":
            print("The following directories are not in $PATH:")
            for d in not_in_path:
                print(f" - '{d}'")
            sys.exit(2)
