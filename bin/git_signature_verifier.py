#!/usr/bin/env python3
import sys
import argparse
import subprocess
import shlex
from typing import Optional, NamedTuple
from enum import Enum
# pip install tabulate
from tabulate import tabulate

RESET = "\033[0m"
RED = "\033[1;31m"
# BLUE="\033[0;94m"#34
YELLOW = "\033[1;33m"
GREEN = "\033[1;32m"

GIT_HASH = "%H"
GIT_SIGNATURE_STATUS = "%G?"
GIT_SIGNATURE_KEY = "%GF"  # the fingerprint of the key used to sign a signed commit
GIT_FORMAT = f"{GIT_SIGNATURE_STATUS},{GIT_SIGNATURE_KEY},{GIT_HASH}"

# @SOURCE: https://git-scm.com/docs/pretty-formats#Documentation/pretty-formats.txt-emGem
# '%G?':
#   "G" for a good (valid) signature,
#   "B" for a bad signature,
#   "U" for a good signature with unknown validity,
#   "X" for a good signature that has expired,
#   "Y" for a good signature made by an expired key,
#   "R" for a good signature made by a revoked key,
#   "E" if the signature cannot be checked (e.g. missing key)
#   and "N" for no signature


class SignatureStatus(Enum):
    Good = "G"
    BadSignature = "B"
    UnknownValidity = "U"
    SignatureExpired = "X"
    KeyExpired = "Y"
    KeyRevoked = "R"
    CheckFailed = "E"
    NoSignature = "N"
    KeyNotInAllowList = "Did I change my key?"
    KeyOnBlockList = "This is bad!"

class KeyStatus(Enum):
    Good = (GREEN, "good")
    Bad = (RED, "bad")
    Unknown = (YELLOW, "unknown")
    ImplicitGood = (GREEN, "")

    def __init__(self, color: str, description: str) -> None:
        self.color = color
        self.description = description

def get_status_color(status: SignatureStatus) -> str:
    if status == SignatureStatus.Good:
        return GREEN
    elif status in [SignatureStatus.NoSignature, SignatureStatus.KeyNotInAllowList]:
        return YELLOW
    else:
        return RED


class CommitInfo(NamedTuple):
    commit_hash: str
    key: str
    signature_status: SignatureStatus


class CommitVerifier:
    def __init__(
        self,
        key_allow_list: list[str],
        key_block_list: list[str],
    ) -> None:
        self.key_allow_list = set(key_allow_list)
        self.key_block_list = set(key_block_list)

        intersection = self.key_allow_list.intersection(self.key_block_list)
        if intersection:
            raise Exception(
                f"The following keys are both blocked and allowed: {', '.join(intersection)}"
            )

    def check_signing_key(self, commit: CommitInfo) -> CommitInfo:
        signature_status = commit.signature_status
        if commit.key:  # ignore unsigned commits
            if self.key_allow_list:
                if commit.key not in self.key_allow_list:
                    signature_status = SignatureStatus.KeyNotInAllowList
            if commit.key in self.key_block_list:
                signature_status = SignatureStatus.KeyOnBlockList
        return commit._replace(signature_status=signature_status)

    def get_key_status(self, key: str) -> KeyStatus:
        if key in self.key_allow_list:
            return KeyStatus.Good
        elif key in self.key_block_list:
            return KeyStatus.Bad
        elif not self.key_allow_list:
            return KeyStatus.ImplicitGood
        else:
            return KeyStatus.Unknown

    def print_key_status(self, commit_list: list[CommitInfo]) -> None:
        if not commit_list:
            return

        key_count = {}
        for commit in commit_list:
            if commit.key:  # ignore unsigned commits
                key_count[commit.key] = key_count.get(commit.key, 0) + 1

        print("Checking signing keys:")
        for key, count in sorted(key_count.items(), key=lambda x: x[1], reverse=True):
            commits = "commit" if count == 1 else "commits"
            key_status = self.get_key_status(key)
            # pylint: disable=no-member
            # Pylint seems to have problems with the enum variables
            color_value = key_status.color
            status = key_status.description

            key = color(color_value, key)
            count = color(color_value, count)
            if status:
                status = " " + color(color_value, status)
            print(f" - {count} {commits} signed with{status} key {key}")


def color(color: Optional[str], msg: str) -> str:
    return f"{color}{msg}{RESET}" if color else msg


def parse_commit(line: str) -> CommitInfo:
    try:
        status, key, commit_hash = line.split(",", maxsplit=2)
        signature_status = SignatureStatus(status)
        return CommitInfo(
            signature_status=signature_status, key=key, commit_hash=commit_hash
        )
    except:
        pass
    raise Exception(f"Failed to parse line as commit: '{line}'")


def get_signing_status(
    repo: str, commit_filter: Optional[str] = None
) -> list[CommitInfo]:
    """
    repo: the path to a git repo
    commit_filter:
        None -> show all commits
        commit -> show all commits before the given commit
        commit_a..commit_b -> show all commits between a and b
        example: main..origin/main (only show fetched (but not merged) commits on branch main)
    """
    # '--pretty="format:%G?"'
    command = ["git", "-C", repo, "log", f"--pretty={GIT_FORMAT}"]
    if commit_filter:
        command.append(commit_filter)

    try:
        output = subprocess.check_output(command)
        lines = output.decode("utf-8").split("\n")
        return [parse_commit(line) for line in lines if line]
    except subprocess.CalledProcessError as e:
        print(f"Command failed with code {e.returncode}:", shlex.join(command))
        print(e.output)
    raise Exception("Git signing check failed")


def print_status_summary(commit_list: list[CommitInfo]) -> None:
    if not commit_list:
        return

    bad_commit_list = [
        x for x in commit_list if x.signature_status != SignatureStatus.Good
    ]
    good = color(GREEN, "good")
    if not bad_commit_list:
        if len(commit_list) == 1:
            print(f"The commit has a {good} signature")
        else:
            total_count = color(GREEN, len(commit_list))
            print(f"All {total_count} commits have {good} signatures")
    else:
        good_count = len(commit_list) - len(bad_commit_list)
        good_count = color(GREEN, good_count)
        total_count = color(None, len(commit_list))
        print(f"{good_count} out of {total_count} commits have {good} signatures")

        bad_type_count = {}
        for x in bad_commit_list:
            key = x.signature_status
            bad_type_count[key] = bad_type_count.get(key, 0) + 1

        for bad_type, count in sorted(
            bad_type_count.items(), key=lambda x: x[1], reverse=True
        ):
            c = get_status_color(bad_type)

            bad_type = color(c, bad_type.name)
            if count == 1:
                one = color(c, count)
                print(f" - {one} commit has status {bad_type}")
            else:
                count = color(c, count)
                print(f" - {count} commits have status {bad_type}")


def show_table(verifier: CommitVerifier, commit_list: list[CommitInfo]) -> None:
    if not commit_list:
        return

    headers = ["Hash (newest commits first)", "Signing key", "Verification status"]
    table = []

    for commit in commit_list:
        css = commit.signature_status
        color_status = get_status_color(css)
        status = color(color_status, css.name)

        if commit.key:
            cks = verifier.get_key_status(commit.key)
            color_key = cks.color
            key = color(color_key, commit.key)
        else:
            key = color(RED, "None")

        table.append([commit.commit_hash, key, status])

    print(tabulate(table, headers, tablefmt="fancy_grid"))


def all_commits_signed_good(commit_list: list[CommitInfo]) -> bool:
    bad_commit_list = [
        x for x in commit_list if x.signature_status != SignatureStatus.Good
    ]
    return not bad_commit_list


def test_data():
    commits = []
    commits += [
        CommitInfo(f"hash_good_{i}", "good_key", SignatureStatus.Good)
        for i in range(10)
    ]
    commits += [
        CommitInfo(f"hash_unknown_{i}", "unknown_key", SignatureStatus.Good)
        for i in range(10)
    ]
    commits += [
        CommitInfo(f"hash_bad_{i}", "bad_key", SignatureStatus.Good) for i in range(5)
    ]
    commits += [
        CommitInfo(f"hash_expired_{i}", "good_key", SignatureStatus.KeyExpired)
        for i in range(3)
    ]
    commits += [
        CommitInfo(f"hash_unsigned_{i}", "", SignatureStatus.NoSignature)
        for i in range(1)
    ]
    return commits


if __name__ == "__main__":
    ap = argparse.ArgumentParser(
        description="This program checks, whether commits to a git repo were signed with valid keys. It will exit with code 2 if problems were found."
    )
    ap.add_argument("repo", help="the path to the git repo")
    ap.add_argument(
        "-g",
        "--good-keys",
        metavar="GOOD_KEY",
        nargs="+",
        help="a list of known good keys",
        default=[],
    )
    ap.add_argument(
        "-b",
        "--bad-keys",
        metavar="BAD_KEY",
        nargs="+",
        help="a list of known bad keys",
        default=[],
    )
    ap.add_argument(
        "-f",
        "--filter",
        help="a filter for 'got log'. Can be a commit hash, branch name, or commit range",
    )
    ap.add_argument(
        "-s",
        "--show-summary",
        action="store_true",
        help="show a summary of the verification",
    )
    ap.add_argument(
        "-t",
        "--show-table",
        action="store_true",
        help="show a table of all commits",
    )
    ap.add_argument(
        "-T", "--test", action="store_true", help="use fake test data and keys"
    )
    args = ap.parse_args()

    if args.test:
        commit_list = test_data()
        verifier = CommitVerifier(["good_key"], ["bad_key"])
    else:
        commit_list = get_signing_status(args.repo, args.filter)
        verifier = CommitVerifier(args.good_keys, args.bad_keys)

    commit_list = [verifier.check_signing_key(x) for x in commit_list]

    if args.show_table:
        show_table(verifier, commit_list)
        if args.show_summary:
            print()

    if args.show_summary:
        print_status_summary(commit_list)
        print()
        verifier.print_key_status(commit_list)

    ok = all_commits_signed_good(commit_list)
    sys.exit(0 if ok else 2)
