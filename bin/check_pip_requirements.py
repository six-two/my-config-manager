#!/usr/bin/env python3
import os
import sys
import argparse
import pkg_resources

def get_pip_install_command(arguments_str: str) -> str:
    return f"python3 -m pip install {arguments_str}"


def test_requirements(path) -> bool:
    """Test that each required package is available."""
    # Ref: https://stackoverflow.com/a/45474387/
    requirements = pkg_resources.parse_requirements(open(path, "r"))
    ok = True
    for requirement in requirements:
        requirement = str(requirement)
        try:
            pkg_resources.require(requirement)
        except Exception:
            print(f"Could not satisfy dependency '{requirement}'")
            ok = False
    return ok


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("pip_requirements_file")
    args = ap.parse_args()

    requirements_path = args.pip_requirements_file
    requirements_path = os.path.realpath(requirements_path)
    requirements_path = os.path.abspath(requirements_path)

    pip_command = get_pip_install_command(f"-r {requirements_path}")
    if not test_requirements(requirements_path):
        print(f"\nPlease run the following command:\n{pip_command}")
        sys.exit(2)


if __name__ == "__main__":
    main()
