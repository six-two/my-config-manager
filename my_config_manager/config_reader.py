import os
# pip install pyyaml
import yaml
from typing import NamedTuple, Callable, Optional
from my_config_manager import ALLOWED_KEYS, PROJECT_ROOT

class ConfigError(Exception):
    pass


class Settings(NamedTuple):
    good_gpg_keys: list[str]
    bad_gpg_keys: list[str]
    is_dev_machine: bool
    project_paths: list[str]


class Hooks(NamedTuple):
    post_install: Optional[str]
    post_update: Optional[str]
    pre_uninstall: Optional[str]


class Dependencies(NamedTuple):
    pip_file: Optional[str]
    pacman_list: list[str]


class Repo(NamedTuple):
    host: str
    path: str
    branch: str
    signed: bool
    public: bool
    can_push: bool
    signing_keys: list[str]


class Config(NamedTuple):
    name: str
    path: str
    add_to_system_path: list[str]
    repo: Repo
    dependencies: Optional[Dependencies]
    optional_deps: Optional[Dependencies]
    hooks: Optional[Hooks]


class PackageGroup(NamedTuple):
    name: str
    config_dir: str
    packages: list[str]


class PackageRepository(NamedTuple):
    groups: list[PackageGroup]
    package_files: dict[str,str]


class PathBuilder:
    def __init__(self):
        self._base_dir: Optional[str] = None
        self._vars = {
            "HOME": os.path.expanduser("~/"),
        }

    def add_path_var(self, name: str, value:str) -> None:
        self._vars[name] = value

    def set_base_dir(self, base_dir: Optional[str]) -> None:
        self._base_dir = base_dir

    def parse_path(self, path: str) -> str:
        return parse_path(path, self._base_dir, self._vars)


def parse_path(path: str, base_dir: Optional[str], variables: dict[str,str]) -> str:
    if not path:
        raise ConfigError("Path is empty")
    parsed = os.path.expanduser(path)

    if parsed.startswith("$"):
        # extract the part between '$' and '/': eg $TEST/abc -> TEST
        split_results = parsed[1:].split("/", maxsplit=1)
        var_name = split_results[0]
        rest_of_path = split_results[1] if len(split_results) > 1 else ""
        value = variables.get(var_name)
        if value:
            parsed = os.path.join(value, rest_of_path)
        else:
            raise ConfigError(f"Used undefined variable '{var_name}' in path")

    if not os.path.isabs(parsed):
        if base_dir:
            # relative paths will be based on "base_dir"
            # This will also allow path traversal ("../", etc)
            parsed = os.path.join(base_dir, parsed)
            parsed = os.path.realpath(parsed)
        else:
            raise ConfigError(f"A relative path was given: '{path}'")
    return parsed


class ConfigParser:
    def __init__(self, data) -> None:
        self.data = data

    def exists(self, key) -> bool:
        do_nothing = lambda x: x
        value = self.get_or_none(do_nothing, key)
        return value != None

    def get_or_none(self, parse_fn: Callable, key: str):
        data = self.data
        for k in key.split("."):
            try:
                data = data[k]
            except KeyError:
                return None
        return parse_fn(data)

    def get_or_default(self, parse_fn: Callable, key: str, default_value):
        value = self.get_or_none(parse_fn, key)
        return value if value != None else default_value

    def get(self, parse_fn: Callable, key: str):
        value = self.get_or_none(parse_fn, key)
        if value == None:
            raise ConfigError(f"Value not defined: {key}")
        else:
            return value


def create_config_parser(path: str) -> ConfigParser:
    with open(path, "r") as f:
        data = yaml.safe_load(f)
    return ConfigParser(data)


def _parse_project_config(parser: ConfigParser, path_builder: PathBuilder()) -> Config:
    name = parser.get(str, "name")
    path = parser.get(str, "path")
    path = path_builder.parse_path(path)

    path_builder.set_base_dir(path)
    path_builder.add_path_var("REPO", path)
    add_to_system_path = parser.get_or_default(list, "add_to_system_path", [])
    add_to_system_path = [path_builder.parse_path(x) for x in add_to_system_path]
    repo = _parse_repo(parser, "repo")
    if parser.exists("dependencies"):
        dependencies = _parse_dependencies(parser, "dependencies", path_builder)
    else:
        dependencies = None

    if parser.exists("optional_deps"):
        optional_deps = _parse_dependencies(parser, "optional_deps", path_builder)
    else:
        optional_deps = None

    if parser.exists("hooks"):
        hooks = _parse_hooks(parser, "hooks", path_builder)
    else:
        hooks = None

    return Config(name=name, path=path, repo=repo, add_to_system_path=add_to_system_path,
                dependencies=dependencies, optional_deps=optional_deps, hooks=hooks)

def _parse_repo(parser: ConfigParser, prefix: str) -> Repo:
    host = parser.get(str, f"{prefix}.host")
    # this is a git path, do NOT proccess it like a path
    path = parser.get(str, f"{prefix}.path")
    branch = parser.get_or_default(str, f"{prefix}.branch", "main")
    # Assume the repo is mine by default
    signed = parser.get_or_default(bool, f"{prefix}.signed", True)
    public = parser.get_or_default(bool, f"{prefix}.public", True)
    can_push = parser.get_or_default(bool, f"{prefix}.can_push", True)
    signing_keys = parser.get_or_default(list, f"{prefix}.signing_keys", ALLOWED_KEYS)

    return Repo(
        host=host,
        path=path,
        branch=branch,
        signed=signed,
        public=public,
        can_push=can_push,
        signing_keys=signing_keys,
    )


def parse_group_config(config_path: str) -> list[PackageGroup]:
    config_dir = os.path.dirname(config_path)
    parser = create_config_parser(config_path)
    groups = []
    unparsed_groups = parser.get_or_default(list, "groups", [])
    for g in unparsed_groups:
        p = ConfigParser(g)
        name = p.get(str, "name")
        packages = p.get(list, "packages")
        groups.append(PackageGroup(name=name, config_dir=config_dir, packages=packages))

    return groups


def parse_repo_folder_recursive(root_dir: str) -> PackageRepository:
    groups = []
    package_files = {}
    exclude_files = ["groups.yaml"]
    for root, dirs, files in os.walk(root_dir):
        yaml_file_names = [x for x in files if x.endswith(".yaml")]
        for yaml_file in yaml_file_names:
            name = yaml_file[:-5] # relative path without extension
            relative_dir = os.path.relpath(root, root_dir)
            pkg_name = os.path.join(relative_dir, name)
            if pkg_name.startswith("./"):
                pkg_name = pkg_name[2:]

            if name == "groups":
                groups += parse_group_config(os.path.join(root, yaml_file))
            else:
                if pkg_name in package_files:
                    raise Exception(f"Repo already contains package: '{pkg_name}'")
                else:
                    package_files[pkg_name] = yaml_file
    
    return PackageRepository(groups=groups, package_files=package_files)


def _parse_dependencies(parser: ConfigParser, prefix: str, path_builder: PathBuilder) -> Dependencies:
    pip_file = parser.get_or_none(str, f"{prefix}.pip_file")
    if pip_file:
        pip_file = path_builder.parse_path(pip_file)
    pacman_list = parser.get_or_default(list, f"{prefix}.pacman_list", [])
    return Dependencies(pip_file=pip_file, pacman_list=pacman_list)


def _parse_hooks(parser: ConfigParser, prefix: str, path_builder: PathBuilder) -> Dependencies:
    post_install = parser.get_or_default(str, f"{prefix}.post_install", None)
    post_update = parser.get_or_default(str, f"{prefix}.post_update", None)
    pre_uninstall = parser.get_or_default(str, f"{prefix}.pre_uninstall", None)
    return Hooks(post_install=post_install, post_update=post_update, pre_uninstall=pre_uninstall)


def parse_project_config(path: str) -> Config:
    path_builder = PathBuilder()
    # The dir in which the config file is stored
    path_builder.add_path_var("CONF", os.path.dirname(path))

    try:
        parser = create_config_parser(path)
        return _parse_project_config(parser, path_builder)
    except Exception:
        raise Exception(f"Failed to parse project config file: '{path}'")


def load_settings() -> Settings:
    path = os.path.expanduser("~/.config/six-two/my-config-manager.yaml")
    if not os.path.exists(path):
        path = os.path.join(PROJECT_ROOT, "config.yaml")
    return parse_settings(path)


def get_real_abs_project_path(path_relative_to_project_root: str) -> str:
    path = os.path.join(PROJECT_ROOT, path_relative_to_project_root)
    return os.path.realpath(path)


def parse_settings(path: str) -> Settings:
    try:
        with open(path, "r") as f:
            data = yaml.safe_load(f)

        parser = ConfigParser(data)
        is_dev_machine = parser.get(bool, "is_dev_machine")
        project_paths = parser.get(list, "project_path")
        project_paths = [get_real_abs_project_path(x) for x in project_paths]
        good_gpg_keys = parser.get(list, "gpg_keys.good")
        bad_gpg_keys = parser.get(list, "gpg_keys.bad")

        return Settings(is_dev_machine=is_dev_machine, project_paths=project_paths,
            good_gpg_keys=good_gpg_keys, bad_gpg_keys=bad_gpg_keys)
    except Exception:
        raise Exception(f"Failed to parse settings file: '{path}'")
