from typing import Optional, Callable, NamedTuple, Union
import traceback
import shlex
import subprocess
from enum import Enum
from my_config_manager import Config

RESET = "\033[0m"
RED = "\033[0;31m"
FAT_RED = "\033[1;31m"
GREEN = "\033[0;32m"
BLUE = "\033[0;34m"
FAT_BLUE = "\033[1;34m"

def color(color: Optional[str], msg: str) -> str:
    return f"{color}{msg}{RESET}" if color else msg


class CheckResult(Enum):
    Good = color(GREEN, "ok")
    Bad = color(RED, "not ok")
    Error = color(FAT_RED, "error")


class OutputSettings(NamedTuple):
    show_commands: bool
    show_command_output: bool
    hide_ok: bool


class SectionOutput:
    def __init__(self, settings: OutputSettings, heading: str) -> None:
        self.settings = settings
        self.heading = heading
        self.check = False
        self.description = None

    def ensure_header_printed(self) -> None:
        if self.heading:
            print(self.heading)
            self.heading = None

    def start_check(self, description: str, command: Optional[list[str]]) -> None:
        if self.check:
            print("[BUG] Previous check not finished")
            self.end_check(CheckResult.Bad, "BUG")

        s = self.settings
        if s.show_commands and command:
            self.ensure_header_printed()
            print("[D] Executing:", shlex.join(command))

        if not s.hide_ok:
            # Show it while running the command
            self.ensure_header_printed()
            print(f"[*] {description}... ", end="", flush=True)
            self.description = None
        else:
            self.description = description
        self.check = True

    def end_check(self, status: CheckResult, command_output: Optional[str]) -> None:
        if self.check:
            self.check = False
            s = self.settings
            if s.hide_ok and status == CheckResult.Good:
                return
            
            self.ensure_header_printed()
            if self.description:
                print(f"[*] {self.description}: {status.value}")
            else:
                print(status.value)

            if s.show_command_output and command_output and status != CheckResult.Good:
                print(command_output)
        else:
            print("[BUG] No check to close")


class HideOutput(SectionOutput):
    def __init__(self) -> None:
        super().__init__(None, None)

    def ensure_header_printed(self) -> None:
        pass

    def start_check(self, description: str, command: Optional[list[str]]) -> None:
        pass

    def end_check(self, status: CheckResult, command_output: Optional[str]) -> None:
        pass


def format_header(config: Config, is_first: bool) -> str:
    if not is_first:
        print() # Add an empty line between projects
    header = f" {config.name} - {config.path} "
    header = color(FAT_BLUE, header)
    return header.center(80, "=")


def run_command(section: SectionOutput, message: str, command: Union[str,list[str]], expected_error_code: int = 0, shell: bool = False) -> bool:
    """
    Run a command. Will hide the output, if the command was successful. 
    Will return True, if the command was successful.
    Prints a line with 'message', followed by whether the process was successful
    """
    section.start_check(message, command)
    try:
        subprocess.check_output(command, stderr=subprocess.STDOUT, shell=shell)
        section.end_check(CheckResult.Good, None)
        return True
    except subprocess.CalledProcessError as e:
        expected = e.returncode == expected_error_code
        output = e.output.decode("utf-8")
        if expected:
            result = CheckResult.Bad
        else:
            result = CheckResult.Error
            output = f"Command exited with code {e.returncode}: {shlex.join(command)}\n" + output
        section.end_check(result, output)
        return False
    except Exception:
        output = traceback.format_exc()
        section.end_check(CheckResult.Error, output)
        return False

