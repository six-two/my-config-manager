import os
import subprocess
import traceback
from typing import Optional
from my_config_manager import Config, Dependencies, run_command, PROJECT_BINS, verify_commits, Settings, get_pull_url, get_push_url
from my_config_manager.output import SectionOutput, CheckResult, HideOutput
from my_config_manager.check import check_project, check_remote_url, check_git_branch

HIDE_OUTPUT = HideOutput()

def fix_remote_url(section: SectionOutput, settings: Settings, config: Config, push_url: bool) -> None:
    if not check_remote_url(HIDE_OUTPUT, settings, config, push_url):
        direction = "push" if push_url else "pull"
        command = ["git", "-C", config.path, "remote", "set-url"]
        if push_url:
            command.append("--push")
            url = get_push_url(config.repo, settings.is_dev_machine)
        else:
            url = get_pull_url(config.repo)
        command += ["origin", url]
        run_command(section, f"Fixing remote {direction} url", command)

def fix_git_branch(section: SectionOutput, settings: Settings, config: Config) -> None:
    if not check_git_branch(HIDE_OUTPUT, config):
        command = ["git", "-C", config.path]
        command += ["checkout", config.repo.branch]
        run_command(section, f"Fixing git branch", command)

def fix_project(section: SectionOutput, settings: Settings, config: Config) -> bool:
    fix_remote_url(section, settings, config, False)
    fix_remote_url(section, settings, config, True)
    fix_git_branch(section, settings, config)

    print()
    return check_project(section, settings, config)
