import os
import subprocess
import shlex
from typing import NamedTuple
from my_config_manager import Config, Repo, Settings, run_command, PROJECT_BINS
from my_config_manager.output import SectionOutput

VERIFIER_SCRIPT = os.path.join(PROJECT_BINS, "git_signature_verifier.py")


def get_https_url(repo: Repo) -> str:
    return f"https://{repo.host}/{repo.path}"


def get_ssh_url(repo: Repo) -> str:
    return f"git@{repo.host}:{repo.path}"


def get_pull_url(repo: Repo) -> str:
    if repo.public:
        # Will not need the ssh key
        return get_https_url(repo)
    else:
        return get_ssh_url(repo)


def get_push_url(repo: Repo, is_dev: bool) -> str:
    if repo.can_push and is_dev:
        # Use ssh url, makes pushing simple
        return get_ssh_url(repo)
    else:
        # Use https url, this should make pushing hard to prevent accidential pushes
        return get_https_url(repo)


def clone_repo(section: SectionOutput, settings: Settings, config: Config) -> bool:
    if os.path.exists(config.path):
        print(f"Folder already exists: {config.path}")
        return False

    pull_url = get_pull_url(config.repo)
    command = ["git", "clone", pull_url, config.path]
    if not run_command(section, "Cloning repo", command):
        return False

    push_url = get_push_url(config.repo, settings.is_dev_machine)
    if pull_url != push_url:
        command = ["git", "-C", config.path]
        command += ["remote", "set-url", "--push", "origin", push_url]
        if not run_command(section, "Changing push url", command):
            return False

    command = ["git", "-C", config.path, "checkout", config.repo.branch]
    if not run_command(section, "Changing branch", command):
        return False

    if not verify_commits(section, settings, config, False):
        return False

    if config.hooks and config.hooks.post_install:
        if not run_command(section, "Running post install hook", config.hooks.post_install, shell=True):
            return False
    return True


def update_repo(section: SectionOutput, settings: Settings, config: Config) -> bool:
    git_base = ["git", "-C", config.path]
    if not run_command(section, "Fetching new commits", [*git_base, "fetch"]):
        return False

    if not verify_commits(section, settings, config, True):
        return False

    command = ["rebase", "@{upstream}"]
    if not settings.is_dev_machine:
        # Prevent it from prompting me for my gpg passphrase
        # Since it is not a dev machine, no changes should ever be pushed
        command.append("--no-gpg-sign")

    if has_new_commits(config):
        if not run_command(section, "Rebasing local branch on origin", [*git_base, *command]):
            return False

        if config.hooks and config.hooks.post_update:
            if not run_command(section, "Running post update hook", config.hooks.post_update, shell=True):
                return False
    return True


def has_new_commits(config: Config) -> bool:
    try:
        command = ["git", "-C", config.path, "log", f"--pretty=%H", "..@{upstream}"]
        output_bytes = subprocess.check_output(command)
        output = output_bytes.decode("utf-8").strip()
        return bool(output)
    except subprocess.CalledProcessError as e:
        # If the git command did not work, something is probably wrong.
        # And in any case, an extra rebase should not do much harm
        return True


def verify_commits(section: SectionOutput, settings: Settings, config: Config, only_new: bool) -> bool:
    if not config.repo.signed:
        return True

    target = "new" if only_new else "all"
    message = f"Verifying {target} commits"
    command = [VERIFIER_SCRIPT, config.path, "--show-summary"]
    if settings.good_gpg_keys:
        command += ["--good-keys", *settings.good_gpg_keys]
    if settings.bad_gpg_keys:
        command += ["--bad-keys", *settings.bad_gpg_keys]
    if only_new:
        command += ["--filter", "..@{upstream}"]

    return run_command(section, message, command, 2)
