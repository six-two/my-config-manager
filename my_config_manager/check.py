import os
import subprocess
import traceback
from typing import Optional
from my_config_manager import Config, Settings, Dependencies, run_command, PROJECT_BINS, verify_commits, get_pull_url, get_push_url
from my_config_manager.output import SectionOutput, CheckResult

CHECK_PATH_SCRIPT = os.path.join(PROJECT_BINS, "check_path.py")
CHECK_PIP_REQUIREMENTS_SCRIPT = os.path.join(PROJECT_BINS, "check_pip_requirements.py")
CHECK_PACMAN_SCRIPT = os.path.join(PROJECT_BINS, "check_pacman_dependencies.py")

def check_command_output_matches(section: SectionOutput, message: str, command: list[str], expected_output: str) -> bool:
    section.start_check(message, command)
    try:
        output = subprocess.check_output(command)
        output = output.decode("utf-8").strip()
        is_same = output == expected_output

        result = CheckResult.Good if is_same else CheckResult.Bad
        final_output = f"Got      : '{output}'\nExpected : '{expected_output}'"
        section.end_check(result, final_output)
        return is_same
    except subprocess.CalledProcessError as e:
        output = e.output.decode("utf-8")
        section.end_check(CheckResult.Error, output)
        return False
    except Exception:
        output = traceback.format_exc()
        section.end_check(CheckResult.Error, output)
        return False

def check_remote_url(section: SectionOutput, settings: Settings, config: Config, push_url: bool) -> bool:
    direction = "push" if push_url else "pull"
    message = f"Checking git {direction} url"
    command = ["git", "-C", config.path, "remote", "get-url", "origin"]
    if push_url:
        command.append("--push")
        expected_output = get_push_url(config.repo, settings.is_dev_machine)
    else:
        expected_output = get_pull_url(config.repo)

    return check_command_output_matches(section, message, command, expected_output)

def check_git_branch(section: SectionOutput, config: Config) -> bool:
    message = "Checking current branch"
    command = ["git", "-C", config.path, "branch", "--show-current"]
    expected_output = config.repo.branch

    return check_command_output_matches(section, message, command, expected_output)

def check_dependencies(section: SectionOutput, deps: Optional[Dependencies], label: str) -> bool:
    ok = True
    if deps:
        if deps.pip_file:
            command = [CHECK_PIP_REQUIREMENTS_SCRIPT, deps.pip_file]
            ok &= run_command(section, f"Checking {label} pip dependencies", command, 2)
        if deps.pacman_list:
            command = [CHECK_PACMAN_SCRIPT, "--packages", *deps.pacman_list]
            ok &= run_command(section, f"Checking {label} pacman dependencies", command, 2)
    return ok

def check_project(section: SectionOutput, settings: Settings, config: Config) -> bool:
    section.start_check("Checking if repo exists", None)
    exists = os.path.exists(config.path)
    result = CheckResult.Good if exists else CheckResult.Bad
    section.end_check(result, None)

    if not exists:
        return False

    ok = True
    ok &= check_remote_url(section, settings, config, False)
    ok &= check_remote_url(section, settings, config, True)
    ok &= check_git_branch(section, config)

    if config.add_to_system_path:
        command = [CHECK_PATH_SCRIPT, *config.add_to_system_path]
        ok &= run_command(section, "Checking if folders are in $PATH", command, 2)

    ok &= check_dependencies(section, config.dependencies, "required")
    ok &= check_dependencies(section, config.optional_deps, "optional")

    ok &= verify_commits(section, settings, config, False)
    return ok
