import os

# The lists should contain the key fingerprints. Leave a list empty to disable it
ALLOWED_KEYS = ["65D1BFEC9FACC7866466CDCA34B9DF73C0BEA9F3"]
BLOCKED_KEYS = []

PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))
PROJECT_ROOT = os.path.realpath(os.path.join(PROJECT_ROOT, ".."))
PROJECT_BINS = os.path.join(PROJECT_ROOT, "bin")

from .config_reader import Repo, Config, Dependencies, Settings, parse_project_config, load_settings
from .output import run_command
from .repo import clone_repo, update_repo, verify_commits, get_pull_url, get_push_url
from .check import check_project
from .fix import fix_project